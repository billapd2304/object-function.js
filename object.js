//soal 1
var motorcycle1 = {    
    brand: "Handa",
    type: "CUB",
    price: 1000
}
console.log(motorcycle1.brand) 
console.log(motorcycle1.type) 

//soal 2
var mobil = [
    {merk: "BMW", warna: "merah", tipe: "sedan"}, 
    {merk: "toyota", warna: "hitam", tipe: "box"}, 
    {merk: "audi", warna: "biru", tipe: "sedan"}
    ]
    console.log(mobil[0].merk)

//soal 3

var studentName = {
    firstName: 'Salsa',
    lastName: 'Billah'
};
 
const {firstName, lastName} = studentName

console.log(firstName)


let person = {name: "Salsa", age: 30}

let newPerson = {...person, hobby: "Eat"}

console.log(newPerson)

 //soal 4
 const fullName = 'John Doe'
 
 const john = {fullName}
 